package com.serebit.strife.buildsrc

/** Versions of dependencies for type-safe consistency. */
object Versions {
    const val COROUTINES: String = "1.3.2"
    const val KTOR: String = "1.2.4"
    const val SERIALIZATION: String = "0.13.0"
    const val LOGKAT: String = "0.4.7"
    const val KLOCK: String = "1.7.3"
    const val JUPITER: String = "5.5.2"
}
